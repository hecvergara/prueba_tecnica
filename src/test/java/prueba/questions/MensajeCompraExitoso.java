package prueba.questions;

import net.serenitybdd.screenplay.Question;
import net.serenitybdd.screenplay.questions.TextContent;
import prueba.userinterface.Comprar;

public class MensajeCompraExitoso {
    /***
     * Aca se plantea la question a ejecutar
     * @return retorna el string sobre el elemento que se indico
     */
    public static Question<String> mensajeCompraRealizada() {
        return actor -> TextContent.of(Comprar.MENSAJECOMPRA_LBL).viewedBy(actor).asString();
    }
}
